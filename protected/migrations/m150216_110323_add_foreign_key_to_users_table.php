<?php

class m150216_110323_add_foreign_key_to_users_table extends CDbMigration
{
	public function safeUp()
	{
		$this->addColumn('users', 'placeId', 'INT(10)');
		$this->addForeignKey('fk_users_placeId__places_placeID', 'users', 'placeId', 'places', 'placeId', 'CASCADE', 'CASCADE');
	}

	public function safeDown()
	{
		$this->dropForeignKey('fk_users_placeId__places_placeID', 'users');
		$this->dropColumn('users', 'placeId');
	}
}