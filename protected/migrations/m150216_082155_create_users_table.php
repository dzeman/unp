<?php

class m150216_082155_create_users_table extends CDbMigration
{
	public function up()
	{
		$sql = "CREATE TABLE `users` (
			`userId` int(10) NOT NULL AUTO_INCREMENT PRIMARY KEY,
			`name` varchar(128) NOT NULL,
			`cleaning` BOOL DEFAULT '0'
		);";
		
		$this->execute($sql);
	}

	public function down()
	{
		$this->dropTable('users');
	}
}