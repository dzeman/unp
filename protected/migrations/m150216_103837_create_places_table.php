<?php

class m150216_103837_create_places_table extends CDbMigration
{
	public function up()
	{
		$sql = "CREATE TABLE `places` (
			`placeId` int(10) NOT NULL AUTO_INCREMENT PRIMARY KEY,
			`name` varchar(128) NOT NULL
		);";
		
		$this->execute($sql);
	}

	public function down()
	{
		$this->dropTable('places');
	}
}