<?php

/**
 * ArrayHelper class file
 */

class ArrayHelper
{
	/**
	 * Element
	 *
	 * Lets you determine whether an array index is set and whether it has a value.
	 * If the element is empty it returns FALSE (or whatever you specify as the default value.)
	 *
	 * @access	public
	 * @param	array	$items
	 * @param	mixed	$array
	 * @param	mixed	$default
	 * @param	bool	$returnDefault
	 * @param	bool	$returnEmptyValues
	 * @return	mixed
	 */
	public static function elements($items, $array, $default = false, $returnDefault = true, $returnEmptyValues = true)
	{
		$return = array();
		if (!is_array($items)) {
			$items = array($items);
		}

		foreach ($items as $item) {
			if (isset($array[$item])) {
				if (!$returnEmptyValues && empty($array[$item])) {
					continue;
				}
				$return[$item] = $array[$item];
			}
			else if ($returnDefault) {
				$return[$item] = $default;
			}
		}
		return $return;
	}

	/**
	 * Elements
	 *
	 * Returns only the array items specified.  Will return a default value if
	 * it is not set.
	 *
	 * @access	public
	 * @param	string	$item
	 * @param	mixed	$array
	 * @param	mixed	$default
	 * @return	mixed	depends on what the array contains
	*/
	public static function element($item, $array, $default = false)
	{
		if (!isset($array[$item]) OR $array[$item] == "") {
			return $default;
		}
		return $array[$item];
	}

	/**
	 * Shortcut for (!empty($data[$key]) ? $data[$key] : $defaultValue;)
	 * or (!empty($data->$key) ? $data->$key : $defaultValue;)
	 *
	 * @access	public
	 * @param	mixed	$data
	 * @param	string	$key
	 * @param	mixed	$defaultValue
	 * @return	void
	 */
	public static function val($data, $key = '', $defaultValue = '')
	{
		if (is_object($data)) {
			// if data is object
			return !empty($data->$key) ? $data->$key : $defaultValue;
		}
		else if (is_array($data)) {
			// if data is array
			return !empty($data[$key]) ? $data[$key] : $defaultValue;
		}
		return $defaultValue;
	}

	/**
	 * Collects all values from the particular column in query
	 *
	 * @access	public
	 * @param	array	$rows
	 * @param	string	$columnName
	 * @return	array
	 */
	public static function columnValues($rows, $columnName)
	{
		$res = array();
		foreach ($rows as $r) {
			$res[] = self::val($r, $columnName);
		}
		return $res;
	}

	/**
	 * Converts query to array
	 *
	 * @access	public
	 * @param	recordset	$query
	 * @param	string		$columnName - column name to be used as key values in result array
	 * @param	bool		$arrayTypeResult - true if output array contain array
	 * @return	array
	 */
	public static function queryToArray($query, $columnName = null, $arrayTypeResult = false)
	{
		$res = array();
		foreach ($query as $r) {
			if (null !== $columnName) {
				if ($arrayTypeResult) {
					$res[self::val($r, $columnName)][] = $r;
				}
				else {
					$res[self::val($r, $columnName)] = $r;
				}
			}
			else {
				$res[] = $r;
			}
		}
		return $res;
	}

	/**
	 * Remove empty values from array
	 *
	 * @access	public
	 * @param	array	$array
	 * @param	string	$column
	 * @return	array
	 */
	public static function removeEmptyValues($array, $column = null)
	{
		if (!is_array($array)) {
			return array();
		}
		if (!empty($column)) {
			return array_filter($array, create_function('$a', 'return !is_null(ArrayHelper::val($a, "'.$column.'", null));'));
		}
		return array_filter($array, create_function('$a', 'return !empty($a);'));
	}

	/**
	 * Set passed value on each element of array
	 *
	 * @access	public
	 * @param	mixed	$array
	 * @param	mixed	$key
	 * @param	mixed	$value
	 * @param	bool	$onEmpty
	 * @return	mixed
	 */
	public static function setValue(&$array, $key, $value, $onEmpty = true)
	{
		if (!is_array($array) || !($count = count($array))) {
			return $array;
		}
		for ($i = 0; $i < $count; $i++) {
			if ($onEmpty) {
				$array[$i] = self::val($array[$i], $key, $value);
			}
			else {
				$array[$i] = $value;
			}
		}
		return $array;
	}

	/**
	 * Алфавитная сортировка элементов.
	 * Возвращает массив: Заглавная буква => массив элементов.
	 *
	 * @access	public
	 * @param	mixed	$data
	 * @param	string	$key
	 * @return	array
	 */
	public static function alphabeticallyListing($data, $key)
	{
		$listing = array();
		foreach ($data as $element) {
			$listing[mb_substr(self::val($element, $key), 0, 1, 'UTF-8')][] = $element;
		}
		return $listing;
	}
}