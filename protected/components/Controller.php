<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{
	public $activeMenu = "";
	
	public function beforeAction($action)
	{
		Yii::app()->clientScript
			->registerCssFile('/public/css/styles.css');
		
		return true;
	}
}