<?php

Yii::import('application.vendors.Bcrypt', true);

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	public $username;
	public $password;
	
	/**
	 * Конструктор
	 * 
	 * @access public
	 * @param string $login
	 * @param string $password
	 * @return void
	 */
	public function __construct($login, $password)
	{
		$this->username	= $login;
		$this->password	= $password;
	}
	
	/**
	 * Authenticates a user.
	 * The example implementation makes sure if the username and password
	 * are both 'demo'.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @param boolean $checkPassword 
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticate($checkPassword = true)
	{		
		if (Yii::app()->params->authData['login'] == $this->username
			&& Yii::app()->params->authData['password'] == $this->password) {
			$this->errorCode 	= self::ERROR_NONE;
		}
		return !$this->errorCode;
	}
}