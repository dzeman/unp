<?php

/**
 * This is the model class for table "users".
 *
 * The followings are the available columns in table 'users':
 * @property string $user_id
 * @property string $email
 * @property string $mobile
 * @property string $city
 * @property string $password
 * @property string $company_title
 */
class Users extends CActiveRecord
{	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'users';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name', 'required'),
			array('name, cleaning', 'safe'),
		);
	}
	
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'place' => array(self::BELONGS_TO, 'Places', 'placeId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'userId'        => 'User',
			'name'			=> 'Имя',
			'cleaning'      => 'Отдел клининга',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('userId',$this->userId,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('cleaning',$this->cleaning,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Users the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public static function getUsers($params = [])
	{
		$query = Yii::app()->db->createCommand()
			->select('userId, name')
			->from('users');
		
		$andWhereCondition = ['and'];
		
		if (!empty($params['placeId'])) {
			$andWhereCondition[] = 'placeId = '.$params['placeId'];
		}
		
		if (!empty(['forBinding'])) {
			$query->select('userId, name, ("1") as count, FALSE as isCleaning');
			$query->where(array_merge($andWhereCondition, ['cleaning = 0']));
			$sql = Yii::app()->db->createCommand()
				->select("NULL, ('Отдел клининга'), COUNT(name) as count, TRUE as isCleaning")
				->from('users')
				->where(array_merge($andWhereCondition, ['cleaning = 1']))
				->group('cleaning')
				->having('COUNT(name) > 0')
				->text;
			
			$query->union($sql);
		}
		
		return $query->setFetchMode(PDO::FETCH_OBJ)->queryAll();
	}
}
