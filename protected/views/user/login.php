<h2>Авторизация</h2>
<?php $form=$this->beginWidget('booster.widgets.TbActiveForm', array(
	'id'			=> 'login-form',
	'htmlOptions' 	=> ['class' => 'well']
)); ?>

	<div>			
		<?=$form->textFieldGroup($model, 'login'); ?>
		<?=$form->error($model, 'login'); ?>
	</div>
	
	<div>			
		<?=$form->passwordFieldGroup($model, 'password'); ?>
		<?=$form->error($model, 'password'); ?>
	</div>

	<button type="submit" class="btn btn-success">ОК</button>

<?php $this->endWidget(); ?>