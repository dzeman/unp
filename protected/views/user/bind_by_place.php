<h2>Привязать к месту</h2>
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'			=> 'bind-user-by-place-form',
	'htmlOptions' 	=> []
)); ?>
	<?php $this->widget(
	    'booster.widgets.TbSelect2',
	    array(
	        'name' => 'place',
	        'data' => $places,
	    )
	);?>
	<br />
	<br />
	<button type="submit" class="btn btn-success">OK</button>

<?php $this->endWidget(); ?>