<h2>Добавить пользователя</h2>
<?php $form=$this->beginWidget('booster.widgets.TbActiveForm', array(
	'id' 			=> 'add-user-form',
	'htmlOptions' 	=> ['class' => 'well']		
)); ?>

	<div>	
		<?=$form->textFieldGroup($model, 'name'); ?>
		<?=$form->error($model, 'name'); ?>
	</div>
	
	<div>
		<?=$form->checkBoxGroup($model, 'cleaning'); ?>
		<?=$form->error($model, 'cleaning'); ?>
	</div>

	<button type="submit" class="btn btn-success">ОК</button>

<?php $this->endWidget(); ?>