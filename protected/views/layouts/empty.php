<!DOCTYPE html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta Accept-Charset="utf-8" />
	<meta name="language" content="ru" />
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body class="header-fixed">
	<?= $content; ?>	
	<div class="clear"></div>
</body>
</html>
