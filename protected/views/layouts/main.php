<!DOCTYPE html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta Accept-Charset="utf-8" />
		<meta name="language" content="ru" />
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
		<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
	
		<title><?php echo CHtml::encode($this->pageTitle); ?></title>
	</head>
	
	<body>				
		<div class="page-content-wrapper">
			<div class="page-content">
				<nav class="navbar navbar-default">
					<div id="navbar" class="navbar-collapse collapse">
			            <ul class="nav navbar-nav">
			             	<li<?='users' == $this->activeMenu ? ' class="active"' : ''?>><a href="/user">Пользователи</a></li>
			              	<li<?='places' == $this->activeMenu ? ' class="active"' : ''?>><a href="/place">Места</a></li>
			              	<li<?='add_user' == $this->activeMenu ? ' class="active"' : ''?>><a href="/user/add_user">Добавить пользователя</a></li>
			              	<li<?='add_place' == $this->activeMenu ? ' class="active"' : ''?>><a href="/place/add_place">Добавить место</a></li>			             
			            </ul>
			            <ul class="nav navbar-nav navbar-right">
			              <li><a href="/user/logout">Выйти</a></li>
			            </ul>
			          </div>
			      </nav>
				<?= $content; ?>
			</div>
		</div>
	</body>
</html>
