<h2>Список мест </h2>
<?php 
$this->widget(
	'booster.widgets.TbGridView',
	array(
		'type' 			=> 'striped',
		'dataProvider' 	=> $places,
		'template' 		=> "{items}{pager}",
		'columns' 		=> $gridColumns,
	)
);
?>