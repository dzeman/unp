<h2>Список пользователей </h2>
<?php 
$this->widget(
	'booster.widgets.TbGridView',
	array(
		'type' 			=> 'striped',
		'dataProvider' 	=> $users,
		'template' 		=> "{items}{pager}",
		'columns' 		=> $gridColumns,
	)
);
?>