<h2>Добавить место</h2>
<?php $form=$this->beginWidget('booster.widgets.TbActiveForm', array(
	'id'			=> 'add-place-form',
	'htmlOptions' 	=> ['class' => 'well']
)); ?>

	<div>	
		<?=$form->textFieldGroup($model, 'name'); ?>
		<?=$form->error($model, 'name'); ?>
	</div>

	<button type="submit" class="btn btn-success">ОК</button>

<?php $this->endWidget(); ?>