<?php

// This is the configuration for yiic console application.
// Any writable CConsoleApplication properties can be configured here.
return array(
	'basePath'	=> dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'		=> 'name',

	// preloading 'log' component
	'preload'=>array('log'),
		
	'import' => array(
			'application.models.*',
			'application.components.*',
			'application.helpers.*',
	),	

	// application components
	'components' => array(
		'db' => array(
			'connectionString' 		=> 'mysql:host=localhost;dbname=name',
			'emulatePrepare' 		=> true,
			'username' 				=> 'user',
			'password' 				=> 'pwd',
			'charset' 				=> 'utf8',
			'enableProfiling'		=> false,
			'enableParamLogging'	=> true,
			'autoConnect'			=> true,
			'schemaCachingDuration'	=> 360000000,
			'schemaCacheID'			=> 'schemaCache'
		),
			
		'log' => array(
			'class'		=> 'CLogRouter',
			'routes'	=> array(
				array(
					'class'		=> 'CFileLogRoute',
					'levels'	=> 'error, warning',
				),
			),
		),
	),
		
	'commandMap' => array(
		'migrate' => array(
				'class'				=> 'system.cli.commands.MigrateCommand',
				'migrationPath'		=> 'application.migrations',
				'migrationTable'	=> 'migrations',
				'connectionID'		=> 'db'
		)
	),
);