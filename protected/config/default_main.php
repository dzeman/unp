<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'			=> dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'				=> 'name',
	'defaultController' => 'user',
	'language'			=> 'ru',

	// preloading 'log' component
	'preload' => array('log', 'booster'),

	// autoloading model and component classes
	'import' => array(
		'application.controllers.*',
		'application.models.*',
		'application.components.*',
		'application.helpers.*',
	),

	// application components
	'components'=>array(
		'user'=>array(
			'allowAutoLogin' 	=> true,
			'class'				=> 'WebUser',
			'returnUrl'			=> '/user',
			'loginUrl'			=> '/user/login'
		),
		// uncomment the following to enable URLs in path-format
		'urlManager'=>array(
			'urlFormat'      => 'path',
			'showScriptName' => false,
			'rules'          => array(
			),
		),
		// uncomment the following to use a MySQL database
		'db'=>array(
			'connectionString' 		=> 'mysql:host=host;dbname=db',
			'emulatePrepare' 		=> true,
			'username' 				=> 'user',
			'password' 				=> 'psw',
			'charset' 				=> 'utf8',
			'enableProfiling'		=> false,
			'enableParamLogging'	=> true,
			'autoConnect'			=> true,
			'schemaCachingDuration'	=> 360000000,
			'schemaCacheID'			=> 'schemaCache'
		),
		'session' => array(
			'class'						=> 'system.web.CDbHttpSession',
			'connectionID'				=> 'db',
			'autoCreateSessionTable'	=> false,
			'sessionTableName'			=> 'sessions',
			'timeout'					=> (3600*24*365),
			'cookieMode'				=> 'allow',
			'cookieParams'				=> array(
				'lifetime' => (3600*24*365),
			),
		),
		'booster' => array(
		    'class'	=> 'ext.booster.components.Booster',
		),
		'errorHandler' => array(
			'errorAction' => '/error',
		),
	),
	'params' => array(
		'authData' => array(
			'login' 	=> 'admin',
			'password' 	=> 'admin1!'
		)
	)
);