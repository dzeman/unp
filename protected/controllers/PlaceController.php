<?php

class PlaceController extends Controller
{	
	public $activeMenu = 'places';
	
	public function filters()
    {
        return [
            'accessControl',
        ];
    }
		
	public function accessRules()
    {
        return array(
            array('deny',
                'actions'	=> ['index', 'add_place', 'users_by_place'],
                'users'		=> ['?'],
            ),
        );
    }

    /**
     * Список мест
     * 
     * @access public
     * @return void
     */
    public function actionIndex()
    {
    	$gridColumns = array(
    		array('name' 	=> 'placeId', 'header'=>'#', 'htmlOptions'=>array('style'=>'width: 60px')),
    		array('value' 	=> 'CHtml::link(CHtml::encode($data->name), "/place/users_by_place/id/$data->placeId")', 'type' => 'html', 'header' => 'Название'),
    	);
    	 
    	$placeData 				= new CArrayDataProvider(Places::model()->findAll());
    	$placeData->keyField 	= 'placeId';
    	$this->render('index', [
    		'places' 		=> $placeData,
    		'gridColumns' 	=> $gridColumns
    	]);
    }
    
    /**
     * Добавляем место
     * 
     * @access public
     * @return void
     */
    public function actionAdd_Place()
    {
    	$this->activeMenu = 'add_place';
    	$model 		= new Places();
    	$placeData 	= Yii::app()->request->getPost('Places', null);
    	if(!empty($placeData)) {
    		$model->attributes 	= $placeData;
    		 
    		if ($model->save()) {
    			$this->redirect('/place');
    		}
    	}
    	$this->render('add_place', ['model' => $model]);
    }
    
    /**
     * Список пользователей, закрепленных за местом
     * 
     * @access public
     * @param int $id
     * @throws CHttpException 404
     * @return void
     */
    public function actionUsers_By_Place($id = null)
    {
    	if (null === $id || null === ($place = Places::model()->findByPk($id))) {
    		throw new CHttpException(404, 'Данного места не существует.');
    	}
    	
    	$gridColumns = array(
    		array('name' 	=> 'userId', 'header'=>'#', 'htmlOptions'=>array('style'=>'width: 60px')),
    		array('value' 	=> '$data->isCleaning ? $data->name." ($data->count)" : $data->name', 'header' => Users::model()->getAttributeLabel('name')),
    	);
    	$userData 			= new CArrayDataProvider(Users::getUsers(['placeId' => $id, 'forBinding' => true]));
    	$userData->keyField	= 'userId';
    	
    	$this->render('users_by_place', [
    		'users' 		=> $userData,
    		'gridColumns' 	=> $gridColumns
    	]);
    }
}