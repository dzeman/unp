<?php
class UserController extends Controller
{		
	public $activeMenu = 'users';
	
	public function filters()
    {
        return [
            'accessControl',
        ];
    }
		
	public function accessRules()
    {
        return array(
            array('deny',
                'actions' 	=> ['index', 'add_user', 'bind_by_place'],
                'users'		=> ['?'],
            ),
        );
    }
    
    /**
     * Список пользователей
     * 
     * @access public
     * @return void
     */
    public function actionIndex()
    {
	     print_r(Yii::app()->user->isGuest);
    	$gridColumns = array(
    			array('name' => 'userId', 'header'=>'#', 'htmlOptions'=>array('style'=>'width: 60px')),
    			array('name' => 'name', 'header' => Users::model()->getAttributeLabel('name')),
    			array('name' => 'cleaning', 'header' => Users::model()->getAttributeLabel('cleaning')),
    			array('value' => 'CHtml::link(CHtml::encode($data->place["name"]), "/place/users_by_place/id/$data->placeId")', 'type' => 'html', 'header' => 'Номер места'),
    			array(
					'value' => 'CHtml::link("Привязать", "/user/bind_by_place/id/$data->userId")',
    				'type'	=> 'html'
    			)
    	);
    	
    	$userData 			= new CArrayDataProvider(Users::model()->findAll());
    	$userData->keyField = 'userId';
    	$this->render('index', [
    		'users' 		=> $userData,
    		'gridColumns' 	=> $gridColumns
    	]);
    }
    
    /**
     * Авторизация пользователя
     * 
     * @access public
     * @return void 
     */
    public function actionLogin()
    {
    	if (!Yii::app()->user->isGuest) {
    		$this->redirect(Yii::app()->user->returnUrl);
    	}
    	$this->layout 	= 'empty';
    	$model 			= new LoginForm();
    	$loginData 		= Yii::app()->request->getPost('LoginForm', null);
    	if(!empty($loginData)) {
    		$model->attributes 	= $loginData;
    			
    		if ($model->validate() && $model->login()) {
    			$this->redirect('/user');
    		}
    	}
    	
    	$this->render('login', ['model' => $model]);
    }
    
    /**
     * Выход
     *
     * @access public
     * @return void
     */
     public function actionLogout()
     {
	     Yii::app()->user->logout();
	     $this->redirect(Yii::app()->user->loginUrl);
     }
    
    /**
     * Добавляем пользовтаеля
     * 
     * @access public
     * @return void
     */
    public function actionAdd_User()
    {
    	$this->activeMenu = 'add_user';
    	$model 		= new Users();
    	$userData 	= Yii::app()->request->getPost('Users', null);
    	if(!empty($userData)) {
    		$model->attributes 	= $userData;
    		 
    		if ($model->save()) {
    			$this->redirect('/user');
    		}
    		else {
    			print_r($mode->getErrors());
    		}
    	}
    	$this->render('add_user', ['model' => $model]);
    }
    
    /**
     * Привязываем пользователя к месту
     * 
     * @access public
     * @param int $id
     * @throws CHttpException 404
     * @return void
     */
    public function actionBind_By_Place($id = null)
    {
    	if (null === $id || null === ($user = Users::model()->findByPk($id))) {
    		throw new CHttpException(404, 'Данного пользователя не существует.');
    	}
    	
    	$placeId = Yii::app()->request->getPost('place');
    	if (!empty($placeId)) {
    		$user->placeId = $placeId;
    		if ($user->save('placeId')) {
    			$this->redirect('/user');
    		}
    	}
    	
    	$this->render('bind_by_place', ['places' => Places::getPlaces()]);
    }
}