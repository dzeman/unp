<?php

/**
 * Error controller class file
 */

class ErrorController extends CController
{
	/**
	 * Error action
	 *
	 * @access public
	 * @return void
	 */
	public function actionIndex()
	{
		$this->layout = 'empty';
		$user = Yii::app()->user;
		if (($error = Yii::app()->errorHandler->error)) {
			if (403 == $error['code']) {
				$this->redirect($user->loginUrl);
			}
			
			$this->render('error', $error);
		}
	}
}